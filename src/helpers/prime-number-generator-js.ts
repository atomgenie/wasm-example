export const primeNumberGenerator = (size: number) => {
    if (size < 2) {
        return []
    }
    const primesArray = new Array<boolean>(size).fill(true)
    primesArray[0] = false
    primesArray[1] = false

    for (let position = 2; position < Math.ceil(Math.sqrt(size)); position++) {
        if (!primesArray[position]) {
            continue
        }
        for (let i = position * position; i < size; i += position) {
            primesArray[i] = false
        }
    }

    return primesArray.reduce<number[]>((prev, curr, index) => {
        if (curr === true) {
            return [...prev, index]
        }
        return prev
    }, [])
}
