#define WASM_EXPORT __attribute__((visibility("default")))
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

extern void writeNumber(long number);

WASM_EXPORT
void *primeNumberGenerator(long size)
{
    if (size < 2)
    {
        return calloc(sizeof(long), 0);
    }

    long *primeArray = calloc(sizeof(long), size);
    long finalSize = size - 2;

    for (long position = 2; position < size / 2; position++)
    {
        if (primeArray[position])
        {
            continue;
        }

        for (long i = position * position; i < size; i += position)
        {
            primeArray[i] = 1;
            finalSize--;
        }
    }

    for (long i = 2; i < size; i++)
    {
        if (!primeArray[i])
        {
            writeNumber(i);
        }
    }
}
