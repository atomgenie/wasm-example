const wasmElm = fetch(`${process.env.PUBLIC_URL}/main.wasm`).then(response =>
    response.arrayBuffer(),
)

export class WasmHelper {
    public async startCompute(size: number): Promise<Array<number>> {
        return new Promise((res, rej) => {
            let instance: any = null
            let memoryStates = new WeakMap()
            let result = new Array<number>()

            function syscall(instance: any, n: any, args: any) {
                switch (n) {
                    default:
                        // console.log("Syscall " + n + " NYI.");
                        break
                    case 45:
                        return 0
                    case 146:
                        return instance.exports.writev_c(args[0], args[1], args[2])
                    case 192:
                        //debugger;
                        const memory = instance.exports.memory
                        let memoryState = memoryStates.get(instance)
                        const requested = args[1]
                        if (!memoryState) {
                            memoryState = {
                                object: memory,
                                currentPosition: memory.buffer.byteLength,
                            }
                            memoryStates.set(instance, memoryState)
                        }
                        let cur = memoryState.currentPosition
                        if (cur + requested > memory.buffer.byteLength) {
                            const need = Math.ceil(
                                (cur + requested - memory.buffer.byteLength) / 65536,
                            )
                            memory.grow(need)
                        }
                        memoryState.currentPosition += requested
                        return cur
                }
            }

            wasmElm
                .then(bytes =>
                    WebAssembly.instantiate(bytes, {
                        js: {
                            mem: new WebAssembly.Memory({
                                initial: 10000,
                                maximum: 65536,
                            }),
                        },
                        env: {
                            __syscall0: function __syscall0(n: any) {
                                return syscall(instance, n, [])
                            },
                            __syscall1: function __syscall1(n: any, a: any) {
                                return syscall(instance, n, [a])
                            },
                            __syscall2: function __syscall2(n: any, a: any, b: any) {
                                return syscall(instance, n, [a, b])
                            },
                            __syscall3: function __syscall3(
                                n: any,
                                a: any,
                                b: any,
                                c: any,
                            ) {
                                return syscall(instance, n, [a, b, c])
                            },
                            __syscall4: function __syscall4(
                                n: any,
                                a: any,
                                b: any,
                                c: any,
                                d: any,
                            ) {
                                return syscall(instance, n, [a, b, c, d])
                            },
                            __syscall5: function __syscall5(
                                n: any,
                                a: any,
                                b: any,
                                c: any,
                                d: any,
                                e: any,
                            ) {
                                return syscall(instance, n, [a, b, c, d, e])
                            },
                            __syscall6: function __syscall6(
                                n: any,
                                a: any,
                                b: any,
                                c: any,
                                d: any,
                                e: any,
                                f: any,
                            ) {
                                return syscall(instance, n, [a, b, c, d, e, f])
                            },
                            writeNumber: function(num: number) {
                                result.push(num)
                            },
                        },
                    }),
                )
                .then(results => {
                    instance = results.instance
                    instance.exports.primeNumberGenerator(size)
                    res(result)
                })
                .catch(e => {
                    rej(e)
                })
        })
    }
}
