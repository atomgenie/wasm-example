import React, { useState } from "react"
import PageHeader from "@atlaskit/page-header"
import Button from "@atlaskit/button"
import { WasmHelper } from "../../helpers/execute-wasm"
import { MAX_VIEW } from "../constants"

const WithWasm: React.FC<{
    size: number
}> = ({ size }) => {
    const [timing, setTiming] = useState<Date | number>()
    const [res, setRes] = useState<Array<number>>([])
    const execute = () => {
        const calculate = async () => {
            const date = new Date().getTime()
            const res = await new WasmHelper().startCompute(size)
            setTiming(new Date().getTime() - date)
            setRes(res)
        }
        calculate()
    }

    return (
        <div>
            <PageHeader>With WASM</PageHeader>
            <div>
                <Button onClick={execute}>Execute</Button>
            </div>
            <br />
            {timing && <div>{timing} ms</div>}
            <div>
                {res
                    .filter((_val, index) => index < MAX_VIEW)
                    .map(elm => (
                        <React.Fragment key={elm}>{elm}, </React.Fragment>
                    ))}
                {res.length > MAX_VIEW && <>...</>}
            </div>
        </div>
    )
}

export default WithWasm
