import React, { useState } from "react"
import PageHeader from "@atlaskit/page-header"
import Button from "@atlaskit/button"
import { MAX_VIEW } from "../constants"
import { primeNumberGenerator } from "../../helpers/prime-number-generator-js"

const WithoutWasm: React.FC<{
    size: number
}> = ({ size }) => {
    const [timing, setTiming] = useState<undefined | number>()
    const [res, setRes] = useState<Array<number>>([])
    const execute = () => {
        const date = new Date().getTime()
        const result = primeNumberGenerator(size)
        setTiming(new Date().getTime() - date)
        setRes(result)
    }
    return (
        <div>
            <PageHeader>Without WASM</PageHeader>
            <div>
                <Button onClick={execute}>Execute</Button>
            </div>
            <br />
            {timing !== undefined && <div>{timing} ms</div>}
            <div>
                {res
                    .filter((_val, index) => index < MAX_VIEW)
                    .map(elm => (
                        <React.Fragment key={elm}>{elm}, </React.Fragment>
                    ))}
                {res.length > MAX_VIEW && <>...</>}
            </div>
        </div>
    )
}

export default WithoutWasm
