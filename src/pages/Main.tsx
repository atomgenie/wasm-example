import React, { useState } from "react"
import DropdownMenu, { DropdownItem, DropdownItemGroup } from "@atlaskit/dropdown-menu"
import Page, { Grid, GridColumn } from "@atlaskit/page"
import PageHeader from "@atlaskit/page-header"
import TextField from "@atlaskit/textfield"
import WithWasm from "./with-wasm/WithWasm"
import WithoutWasm from "./without-wasm/WithoutWasm"

enum OPENED_PAGE {
    NOTHING,
    WITH_WASM,
    WITHOUT_WASM,
}

const Main: React.FC = () => {
    const [openedPage, setOpenedPage] = useState<OPENED_PAGE>(OPENED_PAGE.NOTHING)
    const [size, setSize] = useState(92000)

    return (
        <Page>
            <Grid>
                <GridColumn>
                    <PageHeader>WASM Demo</PageHeader>
                    <TextField
                        value={size}
                        type="number"
                        onChange={(e: any) => {
                            const newNumber = Number(e.target.value)
                            if (newNumber > 92000) {
                                return
                            }
                            setSize(newNumber)
                        }}
                    />
                    <br />
                    <DropdownMenu
                        trigger="Select type"
                        triggerType="button"
                        onItemActivated={() => console.log("YEs")}
                    >
                        <DropdownItemGroup>
                            <DropdownItem
                                onClick={() => setOpenedPage(OPENED_PAGE.WITHOUT_WASM)}
                            >
                                Without WASM
                            </DropdownItem>
                            <DropdownItem
                                onClick={() => setOpenedPage(OPENED_PAGE.WITH_WASM)}
                            >
                                With WASM
                            </DropdownItem>
                        </DropdownItemGroup>
                    </DropdownMenu>
                    {(() => {
                        switch (openedPage) {
                            case OPENED_PAGE.NOTHING:
                                return <></>
                            case OPENED_PAGE.WITHOUT_WASM:
                                return <WithoutWasm size={size} />
                            case OPENED_PAGE.WITH_WASM:
                                return <WithWasm size={size} />
                        }
                    })()}
                </GridColumn>
            </Grid>
        </Page>
    )
}

export default Main
