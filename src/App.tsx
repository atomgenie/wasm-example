import React from "react"
import Main from "./pages/Main"
import "@atlaskit/css-reset"

function App() {
    return <Main />
}

export default App
